/*  
    La MachineALeder / Une Horloge avec un bandeau défilant sur des afficheurs à led 8x8
    Copyright (C) <2018> <TANGUY> <Emmanuel>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// La MachineALeder est une Horloge à LED permettant d'afficher l'heure et un message défilant sur des afficheurs 8x8
// On peut programmer :
//    - la vitesse de défilement du message
//    - l'intensité lumineuse du bandeau
//    - la fréquence d'apparition du message
//    - la durée d'affichage du message

// Contacts :
// Emmanuel TANGUY
// 07 67 62 01 41 / 02 41 86 02 62
// emmanuel.tanguy@laposte.net

// Materiel nécessaire :
// 1 Arduino Nano :                           2€
// 1 Afficheur à LED 4 modules 8x8 MAX7219    3€
// 1 Horloge temps réèl RTC DS3231            1€
//                                           ----
//                                  Total :   6€

// câblage : Afficheur 4 modules 8x8 MAX7219
// Afficheur  -   Arduino Nano
//    CLK             D13
//    CS              D10
//    DIN             D11
//    GND             GND
//    VCC             +5V
//
// Horloge RTC DS3231
//   DS3231   -   Arduino Nano
//    GND             GND
//    VCC             +5V
//    SDA              A4
//    SCL              A5

// Le projet de la machine a LEDER est hébergé sur : 
// https://fabmanager.csc49.fr/#!/projects/la-machine-a-leder


// afficheur à LED 4 x 8x8 ----------------------------------------------------------------------
#include <LEDMatrixDriver.hpp>          // https://github.com/bartoszbielawski/LEDMatrixDriver
uint8_t LEDMATRIX_CS_PIN = 10;          // Affectation de la broche 10 arduino pour le signal CS de l'afficheur

// Definiton des dimensions de la matrice de LED (0-n) - eg: 32x8 = 31x7
int LEDMATRIX_LARGEUR = 31;             // 4 afficheurs de 8 colonnes = 4 x 8 - 1 = 31
int LEDMATRIX_HAUTEUR = 7;              // Nombre de ligne de l'afficheur
int LEDMATRIX_NOMBREAFFICHEURS = 4;     // Nombre d'afficheurs
int len;
int VITESSE_DEFILLEMENT = 27;           // Vitesse se defillement (1 rapide 100 lent etc)
int FREQUENCE_MESSAGE = 27;              // Le message apparait toutes les X minutes
int DUREEMESSAGE = 2;                   // Le message défile durant X minutes

LEDMatrixDriver lmd(LEDMATRIX_NOMBREAFFICHEURS, LEDMATRIX_CS_PIN);    // LEDMatrixDriver class instance



// Horloge Temps Réel RTC_DS3231 ---------------------------------------------------------------------
#include <RTClib.h>
RTC_DS3231 rtc;
int HeureEte, dow, yy, MM, dd, hh, mm, ss;  // dow DayOfWeek - JourDeLaSemaine
int CHANGEMENTHEURE;                        // 0 (changement d'heure non fait)     1 (changement d'heure fait)



// Capteur INTERIEUR (Température et Humidité) GY-BMP280----------------------------------------------
#include <i2c_BMP280.h>
BMP280 bmp280;

double correctionTemperature = -5.3;  // Correction de la temperature Interieure réelle pour ce capteur
double correctionPression = 6.6;      // Correction de la Pression Interieure réelle pour ce capteur
float tempInt;                        // Temperature interieure mesuree
float tempMini;                       // Temperature mini mesurée
float tempMaxi;                       // Temperature maxi mesurée
float pression;                       // Pression atmosphérique mesurée
float presAtmMini;                    // Pression atmosphérique mini mesurée
float presAtmMaxi;                    // Pression atmosphérique maxi mesurée



// ---------------------------------------------------------------------------------------------------------------------
void setup() {
  // -------------------------------------------------------------------------------------------------------------------
  //  Serial.begin(9600);   // init de la com serie (a desactiver car prend de la place memeoire)

  rtc.begin();              // Initialisation de l'horloge RTC temps réel (modifier les 2 lignes si besoin)

  // Pour la mise à l'heure de l'horloge :
  //    1° enlever le commentaire ( // ) de la ligne        // rtc.adjust(...
  //    2° remplacer 2018, 10, 24, 22, 12, 0 correspondant à Année, Mois, Jour, Heure, Minute, Seconde par la Date et Heure courante
  //    3° Téléverser le programme
  //    4° Remettre le commentaire ( // ) devant la ligne    rtc.adjust(...
  //    5° Téléverser le programme

  // -------------------------------------------------------------------------------------------------
  // rtc.adjust(DateTime(2018, 12, 21, 23, 22, 00));       // A décommenter pour Mise a jour de la date et heure si besoin
  // -------------------------------------------------------------------------------------------------

  HeureEte = 1;           //  Reglage auto heure ete/hiver :  HeureEte=0 en hiver & HeureEte=1 en ete
  CHANGEMENTHEURE = 1;    // Changement d'heure possible :    0 = Non    1 = Oui


// Initialisation du BMP280 ----------------------------------------------------
  bmp280.initialize();

  // mesure ponctuelle
  bmp280.setEnabled(0);
  bmp280.triggerMeasurement();

  // initialisation des mini et maxi aux valeurs locales
  bmp280.awaitMeasurement();                     //attendre la fin d'initialisation du compteur
  bmp280.getTemperature(tempInt);
  tempInt = tempInt + correctionTemperature;     // La lecture du capteur exterieur prend 250ms
  bmp280.getPressure(pression);

  pression = pression / 100 + correctionPression;
  presAtmMini = pression;
  presAtmMaxi = pression;
  tempMini = tempInt;
  tempMaxi = tempInt;


  // Matrice de Leds - LEDMATRIX ---------------------------------------------------------------------------------------
  // init the display
  lmd.setEnabled(true);   // Activation de l'afficheur

  // LUMINOSITE
  lmd.setIntensity(10);    // Luminosité de l'afficheur : 0 = faible, 10 = fort   Bright Light
}

// Afficheur MAX7219 ---------------------------------------------------------------------------------------------------
//int x=0, y=0;             // Demarre en Haut à Droite
int x = 32, y = 0;          // Demarre en Haut à Gauche

// Ci-dessous, les matrices de caracteres.
// Outil de creation de vos propres SPRITES sur http://gurgleapps.com/tools/matrix
//
// Exemple :
// Lettre A dessinnée point par point sur une matrice de 8x8
// {0x00, 0x04, 0x0a, 0x11, 0x11, 0x1f, 0x11, 0x11}, // DEC065 0x41, A
//
// 00   0 0 0 0 0 0 0 0
// 04   0 0 0 0 0 X 0 0
// 0A   0 0 0 0 X 0 X 0
// 11   0 0 0 X 0 0 0 X
// 11   0 0 0 X 0 0 0 X
// 1F   0 0 0 X X X X X
// 11   0 0 0 X 0 0 0 X
// 11   0 0 0 X 0 0 0 X


// Création des matrices de caractères ---------------------------------------------------------------------------------

byte font[97][8] = {
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // DEC032 00x20, Space (ascii = 32)
    {0x00, 0x04, 0x04, 0x04, 0x04, 0x04, 0x00, 0x04}, // DEC033 0x21, !
    {0x00, 0x09, 0x09, 0x12, 0x00, 0x00, 0x00, 0x00}, // DEC034 0x22, "
    {0x00, 0x0a, 0x0a, 0x1f, 0x0a, 0x1f, 0x0a, 0x0a}, // DEC035 0x23, #
 // {0x00, 0x04, 0x0f, 0x14, 0x0e, 0x05, 0x1e, 0x04}, // DEC036 0x24, $
    {0x00, 0x3c, 0x42, 0xf8, 0x40, 0xf8, 0x42, 0x3c}, // DEC036 0x24, $ €
    {0x00, 0x19, 0x19, 0x02, 0x04, 0x08, 0x13, 0x13}, // DEC037 0x25, %
 // {0x00, 0x04, 0x0a, 0x0a, 0x0a, 0x15, 0x12, 0x0d}, // DEC038 0x26, &
    {0x00, 0x18, 0x24, 0x19, 0x2a, 0x44, 0x46, 0x39}, // DEC038 0x26, & // &
    {0x00, 0x04, 0x04, 0x08, 0x00, 0x00, 0x00, 0x00}, // DEC039 0x27, '
    {0x00, 0x02, 0x04, 0x08, 0x08, 0x08, 0x04, 0x02}, // DEC040 0x28, (
    {0x00, 0x08, 0x04, 0x02, 0x02, 0x02, 0x04, 0x08}, // DEC041 0x29, )
    {0x00, 0x04, 0x15, 0x0e, 0x1f, 0x0e, 0x15, 0x04}, // DEC042 0x2a, *
    {0x00, 0x00, 0x04, 0x04, 0x1f, 0x04, 0x04, 0x00}, // DEC043 0x2b, +
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x08}, // DEC044 0x2c, ,
    {0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00}, // DEC045 0x2d, -
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x0c}, // DEC046 0x2e, .
    {0x00, 0x01, 0x01, 0x02, 0x04, 0x08, 0x10, 0x10}, // DEC047 0x2f, /
    {0x00, 0x0e, 0x11, 0x13, 0x15, 0x19, 0x11, 0x0e}, // DEC048 0x30, 0
    {0x00, 0x04, 0x0c, 0x04, 0x04, 0x04, 0x04, 0x0e}, // DEC049 0x31, 1
    {0x00, 0x0e, 0x11, 0x01, 0x02, 0x04, 0x08, 0x1f}, // DEC050 0x32, 2
    {0x00, 0x0e, 0x11, 0x01, 0x06, 0x01, 0x11, 0x0e}, // DEC051 0x33, 3
    {0x00, 0x02, 0x06, 0x0a, 0x12, 0x1f, 0x02, 0x02}, // DEC052 0x34, 4
    {0x00, 0x1f, 0x10, 0x1e, 0x01, 0x01, 0x11, 0x0e}, // DEC053 0x35, 5
    {0x00, 0x06, 0x08, 0x10, 0x1e, 0x11, 0x11, 0x0e}, // DEC054 0x36, 6
    {0x00, 0x1f, 0x01, 0x02, 0x04, 0x08, 0x08, 0x08}, // DEC055 0x37, 7
    {0x00, 0x0e, 0x11, 0x11, 0x0e, 0x11, 0x11, 0x0e}, // DEC056 0x38, 8
    {0x00, 0x0e, 0x11, 0x11, 0x0f, 0x01, 0x02, 0x0c}, // DEC057 0x39, 9
    {0x00, 0x00, 0x0c, 0x0c, 0x00, 0x0c, 0x0c, 0x00}, // DEC058 0x3a, :
    {0x00, 0x00, 0x0c, 0x0c, 0x00, 0x0c, 0x04, 0x08}, // DEC059 0x3b, ;
    {0x00, 0x02, 0x04, 0x08, 0x10, 0x08, 0x04, 0x02}, // DEC060 0x3c, <
    {0x00, 0x00, 0x00, 0x1f, 0x00, 0x1f, 0x00, 0x00}, // DEC061 0x3d, =
    {0x00, 0x08, 0x04, 0x02, 0x01, 0x02, 0x04, 0x08}, // DEC062 0x3e, >
    {0x00, 0x0e, 0x11, 0x01, 0x02, 0x04, 0x00, 0x04}, // DEC063 0x3f, ?
    {0x00, 0x0e, 0x11, 0x17, 0x15, 0x17, 0x10, 0x0f}, // DEC064 0x40, @
    {0x00, 0x04, 0x0a, 0x11, 0x11, 0x1f, 0x11, 0x11}, // DEC065 0x41, A
    {0x00, 0x1e, 0x11, 0x11, 0x1e, 0x11, 0x11, 0x1e}, // DEC066 0x42, B
    {0x00, 0x0e, 0x11, 0x10, 0x10, 0x10, 0x11, 0x0e}, // DEC067 0x43, C
    {0x00, 0x1e, 0x09, 0x09, 0x09, 0x09, 0x09, 0x1e}, // DEC068 0x44, D
    {0x00, 0x1f, 0x10, 0x10, 0x1c, 0x10, 0x10, 0x1f}, // DEC069 0x45, E
    {0x00, 0x1f, 0x10, 0x10, 0x1f, 0x10, 0x10, 0x10}, // DEC070 0x46, F
    {0x00, 0x0e, 0x11, 0x10, 0x10, 0x13, 0x11, 0x0f}, // DEC071 0x37, G
    {0x00, 0x11, 0x11, 0x11, 0x1f, 0x11, 0x11, 0x11}, // DEC072 0x48, H
    {0x00, 0x0e, 0x04, 0x04, 0x04, 0x04, 0x04, 0x0e}, // DEC073 0x49, I
    {0x00, 0x07, 0x02, 0x02, 0x02, 0x02, 0x12, 0x0c}, // DEC074 0x4a, J
    {0x00, 0x11, 0x12, 0x14, 0x18, 0x14, 0x12, 0x11}, // DEC075 0x4b, K
    {0x00, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x1f}, // DEC076 0x4c, L
    {0x00, 0x11, 0x1b, 0x15, 0x11, 0x11, 0x11, 0x11}, // DEC077 0x4d, M
    {0x00, 0x11, 0x11, 0x19, 0x15, 0x13, 0x11, 0x11}, // DEC078 0x4e, N
    {0x00, 0x0e, 0x11, 0x11, 0x11, 0x11, 0x11, 0x0e}, // DEC079 0x4f, O
    {0x00, 0x1e, 0x11, 0x11, 0x1e, 0x10, 0x10, 0x10}, // DEC080 0x50, P
    {0x00, 0x0e, 0x11, 0x11, 0x11, 0x15, 0x12, 0x0d}, // DEC081 0x51, Q
    {0x00, 0x1e, 0x11, 0x11, 0x1e, 0x14, 0x12, 0x11}, // DEC082 0x52, R
    {0x00, 0x0e, 0x11, 0x10, 0x0e, 0x01, 0x11, 0x0e}, // DEC083 0x53, S
    {0x00, 0x1f, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04}, // DEC084 0x54, T
    {0x00, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x0e}, // DEC085 0x55, U
    {0x00, 0x11, 0x11, 0x11, 0x11, 0x11, 0x0a, 0x04}, // DEC086 0x56, V
    {0x00, 0x11, 0x11, 0x11, 0x15, 0x15, 0x1b, 0x11}, // DEC087 0x57, W
    {0x00, 0x11, 0x11, 0x0a, 0x04, 0x0a, 0x11, 0x11}, // DEC088 0x58, X
    {0x00, 0x11, 0x11, 0x0a, 0x04, 0x04, 0x04, 0x04}, // DEC089 0x59, Y
    {0x00, 0x1f, 0x01, 0x02, 0x04, 0x08, 0x10, 0x1f}, // DEC090 0x5a, Z
    {0x00, 0x0e, 0x08, 0x08, 0x08, 0x08, 0x08, 0x0e}, // DEC091 0x5b, [
    {0x00, 0x10, 0x10, 0x08, 0x04, 0x02, 0x01, 0x01}, // DEC092 0x5c, \ 
 // {0x00, 0x0e, 0x02, 0x02, 0x02, 0x02, 0x02, 0x0e}, // DEC093 0x5d, ]
    {0x06, 0x09, 0x09, 0x06, 0x00, 0x00, 0x00, 0x00}, // DEC093 0x5d, ] °
 // {0x00, 0x04, 0x0a, 0x11, 0x00, 0x00, 0x00, 0x00}, // DEC094 0x5e, ^
 // {0x10, 0x38, 0x54, 0x92, 0x10, 0x12, 0x37, 0x12}, // DEC094 0x5e, ^ fleche haut
    {0xd8, 0xa8, 0x88, 0x88, 0x20, 0x55, 0x72, 0x55}, // DEC094 0x5e, ^ fleche haut 
 // {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1f}, // DEC095 0x5f, _
 // {0x10, 0x17, 0x10, 0x10, 0x92, 0x54, 0x38, 0x10}, // DEC095 0x5f, _ fleche bas
    {0xd8, 0xa8, 0x88, 0x88, 0x20, 0x2D, 0x2B, 0x29}, // DEC095 0x5f, _ fleche bas
 // {0x00, 0x04, 0x04, 0x02, 0x00, 0x00, 0x00, 0x00}, // DEC096 0x61,   
    {0x00, 0x04, 0x04, 0x02, 0x00, 0x00, 0x00, 0x00}, // DEC096 0x60, `
    {0x00, 0x00, 0x00, 0x1c, 0x02, 0x1e, 0x12, 0x1e}, // DEC097 0x61, a
    {0x00, 0x10, 0x10, 0x10, 0x1c, 0x12, 0x12, 0x1c}, // DEC098 0x62, b
    {0x00, 0x00, 0x00, 0x0e, 0x10, 0x10, 0x10, 0x0e}, // DEC099 0x63, c
    {0x00, 0x01, 0x01, 0x01, 0x07, 0x09, 0x09, 0x07}, // DEC100 0x64, d
    {0x00, 0x00, 0x00, 0x0c, 0x12, 0x1c, 0x10, 0x0e}, // DEC101 0x65, e
    {0x00, 0x06, 0x09, 0x08, 0x1c, 0x08, 0x08, 0x08}, // DEC102 0x66, f
    {0x00, 0x00, 0x00, 0x0c, 0x12, 0x0e, 0x02, 0x1c}, // DEC103 0x67, g
    {0x00, 0x10, 0x10, 0x10, 0x16, 0x19, 0x11, 0x11}, // DEC104 0x68, h
    {0x00, 0x00, 0x04, 0x00, 0x0c, 0x04, 0x04, 0x0e}, // DEC105 0x69, i
    {0x00, 0x02, 0x00, 0x06, 0x02, 0x02, 0x12, 0x0c}, // DEC106 0x6a, j
    {0x00, 0x10, 0x10, 0x12, 0x14, 0x18, 0x14, 0x12}, // DEC107 0x6b, k
    {0x00, 0x0c, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04}, // DEC108 0x6c, l
    {0x00, 0x00, 0x00, 0x0a, 0x15, 0x15, 0x11, 0x11}, // DEC109 0x6d, m
    {0x00, 0x00, 0x00, 0x16, 0x19, 0x11, 0x11, 0x11}, // DEC110 0x6e, n
    {0x00, 0x00, 0x00, 0x0e, 0x11, 0x11, 0x11, 0x0e}, // DEC111 0x6f, o
    {0x00, 0x00, 0x1c, 0x12, 0x12, 0x1c, 0x10, 0x10}, // DEC112 0x70, p
    {0x00, 0x00, 0x07, 0x09, 0x09, 0x07, 0x01, 0x01}, // DEC113 0x71, q
    {0x00, 0x00, 0x00, 0x16, 0x19, 0x10, 0x10, 0x10}, // DEC114 0x72, r
    {0x00, 0x00, 0x00, 0x0f, 0x10, 0x0e, 0x01, 0x1e}, // DEC115 0x73, s
 // {0x00, 0x00, 0x00, 0x0c, 0x10, 0x08, 0x04, 0x18}, // DEC115 0x73, s s
    {0x00, 0x08, 0x08, 0x1c, 0x08, 0x08, 0x09, 0x06}, // DEC116 0x74, t
    {0x00, 0x00, 0x00, 0x11, 0x11, 0x11, 0x13, 0x0d}, // DEC117 0x75, u
    {0x00, 0x00, 0x00, 0x11, 0x11, 0x11, 0x0a, 0x04}, // DEC118 0x76, v
    {0x00, 0x00, 0x00, 0x11, 0x11, 0x15, 0x15, 0x0a}, // DEC119 0x77, w
    {0x00, 0x00, 0x00, 0x11, 0x0a, 0x04, 0x0a, 0x11}, // DEC120 0x78, x
    {0x00, 0x00, 0x00, 0x22, 0x14, 0x08, 0x08, 0x08}, // DEC121 0x79, y
    {0x00, 0x00, 0x00, 0x1f, 0x02, 0x04, 0x08, 0x1f}, // DEC122 0x7a, z
 // {0x00, 0x06, 0x08, 0x08, 0x10, 0x08, 0x08, 0x06}, // DEC123 0x7b, {
    {0x3c, 0x42, 0xa5, 0x81, 0xa5, 0x99, 0x42, 0x3c}, // DEC123 0x7b, { Smilley souriant
 // {0x00, 0x04, 0x04, 0x04, 0x00, 0x04, 0x04, 0x04}, // DEC124 0x7c, |
    {0x08, 0x04, 0x00, 0x0C, 0x12, 0x1C, 0x10, 0x0E}, // DEC124 0x7c, | è
 // {0x00, 0x0c, 0x02, 0x02, 0x01, 0x02, 0x02, 0x0c}, // DEC125 0x7d, }
    {0x10, 0x08, 0x00, 0x1c, 0x04, 0x1c, 0x14, 0x1e}, // DEC125 0x7d  } à
 // {0x00, 0x08, 0x15, 0x02, 0x00, 0x00, 0x00, 0x00}, // DEC126 0x7e, ~
    {0x04, 0x08, 0x00, 0x0c, 0x12, 0x1c, 0x10, 0x0e}, // DEC126 0x7e, ~ é
    {0x00, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f}, // DEC127 0x7f, DEL

  /*
    {0x3c, 0x42, 0xa5, 0x99, 0x81, 0x99, 0x42, 0x3c}, // Smilley   (+31)
    {0x3c, 0x42, 0xa5, 0x81, 0xa5, 0x99, 0x42, 0x3c}, // Smilley souriant
    {0x40, 0x38, 0x46, 0x81, 0x85, 0x4a, 0x50, 0x50}, // Chameau cool
    {0x03, 0x73, 0xa8, 0x20, 0x4e, 0x55, 0x84, 0x88}, // Palmier pourri avec soleil carré ?
    {0x00, 0x10, 0x08, 0x04, 0x7e, 0x04, 0x08, 0x10}, // <- fleche gauche
    {0x00, 0x08, 0x10, 0x20, 0x7f, 0x20, 0x10, 0x10}, // -> fleche droite
    {0x00, 0x36, 0x49, 0x41, 0x41, 0x22, 0x14, 0x08}, // Coeur
    {0x00, 0x0c, 0xcc, 0x0e, 0x02, 0x0e, 0x0a, 0x0f}, // à
    {0x04, 0x08, 0x00, 0x0c, 0x12, 0x1c, 0x10, 0x0e}, // é
    {0x08, 0x14, 0x22, 0x41, 0x22, 0x14, 0x08, 0x00}, // losange
    {0x1e, 0x21, 0x49, 0x9d, 0x89, 0x81, 0x7e, 0x66}, // Camion croix rouge gauche cool
    {0x7e, 0x81, 0xa5, 0xa5, 0x81, 0xa5, 0x99, 0x7e}, // Bn
  */
};








// -----------------------------------------------------------------------------------------------------
void loop() {
// -----------------------------------------------------------------------------------------------------
  char text[207];  // Taille maximum du message

  // Acquisition des mesures ------------------------------------------------------------

  if (mm % FREQUENCE_MESSAGE == 0 && ss == 0)       // Acquisition des mesure lancée au debut de diffusion du message 
  {
    bmp280.awaitMeasurement();      // passage du capteur en mode lecture 
    bmp280.getTemperature(tempInt); // lecture de la temperature intérieure (BPM280)
    tempInt = tempInt + correctionTemperature;
    bmp280.getPressure(pression);   // lecture de la Pression Atmospherique (BPM280)
    pression = pression / 100 + correctionPression;
    bmp280.triggerMeasurement();    // remise de capteur en mode repos 
  }

  /*
    // Calcul de l'altitude (BPM280)
        static float meters, metersold;
        bmp280.getAltitude(meters);
        metersold = (metersold * 10 + meters)/11;
        bmp280.triggerMeasurement();
  */

  // Enregistrement des minis et Maxi --------------------------------------------------
  
  if (tempMini > tempInt) {         // enregistrement de la Temp mini
    tempMini = tempInt;
  }
  if (tempMaxi < tempInt) {         // enregistrement de la Temp maxi
    tempMaxi = tempInt;
  }
  if (presAtmMini > pression) {     // enregistrement de la Pression maxi
    presAtmMini = pression;
  }
  if (presAtmMaxi < pression) {     // enregistrement de la Pression mini
    presAtmMaxi = pression;
  }


  // Gestion de l'horloge RTC // -----------------------------------------------------------------------

  // Lecture de l'horloge
      DateTime now = rtc.now();
      yy = now.year(), DEC;
      MM = now.month(), DEC;
      dd = now.day(), DEC;
      hh = now.hour(), DEC;
      mm = now.minute(), DEC;
      ss = now.second(), DEC;
      dow = now.dayOfTheWeek(), DEC;


  // Initialisation de l'indicateur Heure d'hiver avant le 22 mars et après le 28 octobre   HeureEte=0:hiver / HeureEte=1:été

      if ((MM < 3 ) or ((MM == 3) and (dd < 22)) or ((MM == 10) and (dd > 28)) or (MM > 10))     // nécéssaire si pas de memorisation de HeureEte dans la NVRAM
      {
        HeureEte = 0;                       // On passe en heure d'hiver -> heure d'hiver = 0 & HeureEte = 1
        CHANGEMENTHEURE = 1;                // Autorisation du changement d'heure
      }
    
      if ((MM > 3 && MM < 10 ) or ((MM == 3) and (dd < 22)) or ((MM == 10) and (dd > 28)))       // nécéssaire si pas de memorisation de HeureEte dans la NVRAM
      {
        HeureEte = 1;                       // On passe en heure d'été -> heure d'hiver 0 & HeureEte = 1
        CHANGEMENTHEURE = 1;                // Autorisation du changement d'heure
      }

  // Passage a l'heure d'hiver

      if (CHANGEMENTHEURE == 1 && HeureEte == 1 && dow == 0 && MM == 10 && dd >= 22 && dd <= 28 && hh == 3 && mm <= 2)      // Si on est le 4eme dimanche du mois
      {
        rtc.adjust(DateTime(yy, MM, dd, 2, mm, ss)); // Initialise l'orloge RTC à 2h00
        HeureEte = 0;                            // A stocker dans une NVRAM (si on en a une... :-(  )
        CHANGEMENTHEURE = 0;                     // Blocage du changement d'heure jusqu'au 28/10
      }

  // Passage a l'heure d'été
    
      if (CHANGEMENTHEURE == 1 && HeureEte == 0 && dow == 0 && MM ==  3 && dd >= 22 && dd <= 28 && hh == 2 && mm <= 2)        // Si on est le 4eme dimanche du mois
      {
        rtc.adjust(DateTime(yy, MM, dd, 3, mm, ss)); // Initialise l'orloge RTC à 3h00
        HeureEte = 1;                             // A stocker dans une NVRAM (si on en a une... :-(  )
        CHANGEMENTHEURE = 0;                      // Blocage du changement d'heure jusqu'au 28/10
      }
    
      char *jour[7] = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"};
    

  char tempfull[5];
  int tm = tempMini;
  int tM = tempMaxi;
  // mise en forme de la température avec décimale car les float ne sont pas traités par sprintf dans ARDUINO :-(  ---------------------------
  sprintf(tempfull, "%d.%01d", (int)tempInt, abs((int)(tempInt*10)%10)); 
  
  char pressfull[7];
  int p = pression;
  int pm = presAtmMini;
  int pM = presAtmMaxi;
  sprintf(pressfull, "%d.%01d", (int)pression, abs((int)(pression*10)%10)); 
     
  // sprintf(text, " %s %02d/%02d/%02d %02d:%02d:%02d    %02dhPa    %02d C           ", pression, jour[dow], dd, MM, yy, hh, mm, ss, pression, tempInt);      // message par defaut (GroupeJourDateHeure)
  sprintf(text, " %s %02d/%02d/%02d %02d:%02d:%02d    %s]C    _%d]C ^%d]C    %smB    _%dmB ^%dmB   ", jour[dow], dd, MM, yy, hh, mm, ss, tempfull, tm, tM, pressfull, pm, pM);      // message par defaut (GroupeJourDateHeure)
  len = strlen(text);



  
  // boucle qui joue le message de pub --------------------------------------------------------------------
  for (int n = 0 ; n <= DUREEMESSAGE - 1; n++)      // Nombre de minutes pendant lesquelles on joue le message par defaut
  {
    if ((mm - n) % FREQUENCE_MESSAGE == 0)
    {
      // Gestion des messages à afficher ------------------------------------------------------------------
      //               00                                                                                                  10                                                                                                  20                                                                                                  30
      //               0000000000111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111112222222222333333333344444444445555555555666666666677777777778888888888999999999900000000001111111111222222222233333333334444444444555555555566666666667777777777888888888899999999990000000000111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999
      // Messages en attente  (385 caracteres maxi)
      // strcpy(text, " Bienvenue au Boc@l le FABLAB de Chemill~    bocal.csc49.fr    Impression 3D, D~coupe Lazer, ARDUINO, Repair caf~ ...    Atelier \"Hackers\" tous les jeudi soir } 20h00    Contact YVAN au 02 41 30 50 55    Demandez votre Horloge avec messages personnalis~s } machinealeder@laposte.net ");
      // strcpy(text, " Bienvenue } la DES de l'~cole du G~nie(49)    RDV pour une formation tres intensive ...    Assurez votre reconversion dans un metier d'avenir    Math, Elec, RDM, Routes, Eau, Clim, march~s Publics, ...    Pour plus d'information, la DES de l'~cole du G~nie vous attend au 02 41 24 84 27 ");
      // strcpy(text, " Bienvenue } l'~cole du G~nie (Angers)    Assurez votre reconversion dans un m~tier d'avenir par une formation intensive : Math, Elec, RDM, Routes, Eau, Clim, ...    contactez la DES au 02 41 24 84 27   ");
      // strcpy(text, " Bienvenue } l'~cole du G~nie (Angers)    Assurez votre reconversion dans un m~tier d'avenir par une formation intensive : Elec, RDM, Routes, Eau, Clim, ...  Contactez la DES au 02 41 24 84 27 ");
      // strcpy(text, " Demandez votre Horloge avec messages personnalis~s { Emmanuel au 07 67 62 01 41 ou par mail : machinealeder@laposte.net "};
         strcpy(text, " Joyeux Noel 2018 Nicolas et bonne ann~e 2019. Grosse ann~e avec le BAC, tes 18 ans, peut etre un d~part loin de Durtal et une nouvelle vie qui commence, la tienne ! Bises de nous deux, Myriam & Emmanuel    ");
      // strcpy(text, " Bienvenue } l'~cole du G~nie (Angers)    Assurez votre reconversion dans un m~tier d'avenir par une formation intensive : Math, Elec, RDM, Routes, Eau, Clim, ...    contactez la DES au 02 41 24 84 27   ");
      // strcpy(text, " Bienvenue au Boc@l le FABLAB de Chemill~    bocal.csc49.fr    Impression 3D, D~coupe Lazer, ARDUINO, Repair caf~ ...   "); // Atelier \"Arduino\" tous les jeudi soir } 20h00    Contact YVAN au 02 41 30 50 55 "); //   Demandez votre Horloge avec messages personnalis~s } machinealeder@laposte.net ");
      // strcpy(text, " CONNECTED WEEK AVRILLE  {  La machine } LEDER  prix : 6 $  par emmanuel.tanguy@laposte.net  Projet du FABLAB de Chemill~ (bocal.csc49.fr)   Retrouvez aussi, sur ce stand, le FABLAB de la Meignanne  "); //     Projet La machine a LEDER par F4AWZ     Impression 3D, D~coupe Lazer, ARDUINO, Repair caf~ ...   Atelier \"Arduino\" tous les jeudi soir } 20h00    Contact  "); //   Demandez votre Horloge avec messages personnalis~s } machinealeder@laposte.net ");
      // strcpy(text, " Sainte Anne, o bonne m|re, toi que nous implorons, entends notre pri|re, et b~nis tes Bretons   ");
      // strcpy(text, " Bonne ann~e 2019 avec l'ARML, l'association des VRAIS Radioamateurs    contact : 06 16 19 78 05    radioamateurs.arml@gmail.com    RDV chaque 1er mercredi du mois 20h30 au 3 rue de la Rame, 49100 Angers   ");

      len = strlen(text);
      // VITESSE_DEFILLEMENT = VD;
    }
  }

  // affichage du bandeau "version" pendant les 16 secondes qui suivent la mise sous tension ------------------------------------
  if ( millis() <= 18000 )            // 16000 milli secondes = 16 secondes
  {
         strcpy(text, " La machine } LEDER v0.98 du 24/12/2018 emmanuel.tanguy@laposte.net    ");
  }
  
  drawString(text, len, x, 0);      // Affiche le texte à la position courante
  // Au cas ou vous ne voulez pas utiliser lmd.clear() dans chaque boucle: Le caractere à un fond opaque (Noir)...
  lmd.display();                    // Toggle display of the new framebuffer
  delay(VITESSE_DEFILLEMENT);       // Attente pour laisser à l'utilisateur le temps de lire l'afficheur

  // Advance à la prochaine coordonnee
  if ( --x < len * -8 )
  {
    x = LEDMATRIX_LARGEUR;
  }
  // }
}


// ---------------------------------------------------------------------------------------------------------------------
// Fonctions
// ---------------------------------------------------------------------------------------------------------------------

// Cette fonction dessine la chaine de caracteres de la longueur donnée, à la position donnée
    void drawString(char* text, int len, int x, int y )
    {
      for ( int idx = 0; idx < len; idx ++ )
      {
        int c = text[idx] - 32;     // 32 = 32 eme position du code ascii : premiere lettre de mon tableau de SPRITES
        // Arret si le caractere est en dehors de la zone visible
        if ( x + idx * 8  > LEDMATRIX_LARGEUR ) 
        {
          return;
        }
        // Affiche si le caractere est visible
        if ( 8 + x + idx * 8 > 0 ) 
        {
          drawSprite( font[c], x + idx * 8, y, 8, 8 );
        }
      }
    }


// Dessine le sprite à la position donnée en utilisant la largeur et la hauteur supportée (en general : 8x8)
    void drawSprite( byte* sprite, int x, int y, int width, int height )
    {
      byte mask = B10000000;      // Le masque est utilisé pour obtenir le bit de la colonne depuis la ligne du SPRITE
      for ( int iy = 0; iy < height; iy++ )
      {
        for ( int ix = 0; ix < width; ix++ )
        {
          lmd.setPixel(x + ix, y + iy, (bool)(sprite[iy] & mask ));
          mask = mask >> 1;      // Decale le masque de 1 pixel vers la droite
        }
        mask = B10000000;        // reset du masque Colonne
      }
    }

